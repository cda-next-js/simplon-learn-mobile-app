export default interface School {
    id: string;
    name: string;
    location: string;
    city: string;
    zipCode: string;
    latitude: number;
    longitude: number;
    description: string;
  }

  