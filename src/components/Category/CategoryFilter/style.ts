import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    filterContainer: {
        paddingHorizontal: 15,
        flexDirection: 'row',
      },
      categoryFilter: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
      },
});

export default styles;